const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click', function(){

    let valorAuto = document.getElementById('txtValorAuto').value;
    let porcentaje = document.getElementById('txtPorcentaje').value;
    let plazo = document.getElementById('plazos').value;

    let pagoInicial = valorAuto * (porcentaje/100);
    let totalFin = valorAuto - pagoInicial;
    let plazos = totalFin/plazo;

    document.getElementById('txtPagoInicial').value = pagoInicial;
    document.getElementById('txtTotalFin').value = totalFin;
    document.getElementById('txtPagoMensual').value = plazos;

});

const btnLimpiar = document.getElementById('btnLimpiar');
btnLimpiar.addEventListener('click', function(){

    txtPagoInicial.value = "";
    txtPagoMensual.value = "";
    txtTotalFin.value = "";
    txtValorAuto.value = "";
    txtPorcentaje.value = "";

});

function limpiarParrafo() {
    let btnLimpiar = document.getElementById('parrafo');
    parrafo.innerHTML = "";
}