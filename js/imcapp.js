const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click', function(){

    let valorAltura = document.getElementById('txtAltura').value;
    let valorPeso = document.getElementById('txtPeso').value;

    let alturaCuadrado = valorAltura * valorAltura;
    let IMC = valorPeso/alturaCuadrado;

    document.getElementById('valorFinal').value = IMC.toFixed(2);
    imagenIMC(IMC);
    totalCalorias(valorPeso);

});

function imagenIMC(IMC) {
    let img=document.getElementById('imagen');

    let sexoM = document.getElementById('radMas').checked;
    let sexoF = document.getElementById('radFem').checked;

    if(sexoM){
        if(IMC<18.5) {
            img.src="../img/01H.png"
        } else if (18.5<=IMC && IMC<=24.9) {
            img.src="../img/02H.png"
        } else if (25<=IMC && IMC<=29.9) {
            img.src="../img/03H.png"
        } else if (30<=IMC && IMC<=34.9) {
            img.src="../img/04H.png"
        } else if (35<=IMC && IMC<=39.9) {
            img.src="../img/05H.png"
        } else {
            img.src="../img/06H.png"
        }
    }

    if(sexoF){
        if(IMC<18.5) {
            img.src="../img/01M.png"
        } else if (18.5<=IMC && IMC<=24.9) {
            img.src="../img/02M.png"
        } else if (25<=IMC && IMC<=29.9) {
            img.src="../img/03M.png"
        } else if (30<=IMC && IMC<=34.9) {
            img.src="../img/04M.png"
        } else if (35<=IMC && IMC<=39.9) {
            img.src="../img/05M.png"
        } else {
            img.src="../img/06M.png"
        }
    }
}

function totalCalorias(valorPeso) {
    let edad = document.getElementById('txtEdad').value;
    let calorias = 0;

    let sexoM = document.getElementById('radMas').checked;
    let sexoF = document.getElementById('radFem').checked;

    if(sexoM) {
        if(10<=edad && edad<=17) {
            calorias = (17.686 * valorPeso) + 658.2;
        } else if(18<=edad && edad<=29) {
            calorias = (15.057 * valorPeso) + 692.2;
        } else if(30<=edad && edad<=59) {
            calorias = (11.472 * valorPeso) + 873.1;
        } else {
            calorias = (11.711 * valorPeso) + 587.7;
        }
    }

    if(sexoF) {
        if(10<=edad && edad<=17) {
            calorias = (13.384 * valorPeso) + 692.6;
        } else if(18<=edad && edad<=29) {
            calorias = (14.818 * valorPeso) + 486.6;
        } else if(30<=edad && edad<=59) {
            calorias = (8.126 * valorPeso) + 845.6;
        } else {
            calorias = (9.082 * valorPeso) + 685.5;
        }
    }

    document.getElementById('txtCalorias').value = calorias.toFixed(2);

}