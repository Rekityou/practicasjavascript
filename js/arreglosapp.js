const btnGenerar = document.getElementById('btnGenerar');
btnGenerar.addEventListener('click', function(){

    numGenerator();
    saltarError();

});

function numGenerator(){
    var limite = document.getElementById('limite').value;
    var Listanumeros = document.getElementById('numeros');
    let par = document.getElementById('pares');
    let imp = document.getElementById('impares');
    let simetria = document.getElementById('simetrico');
    let mayor = document.getElementById('mayor');
    let mayorPos = document.getElementById('mayorPos');
    let menor = document.getElementById('menor');
    let menorPos = document.getElementById('menorPos');
    let promedio = document.getElementById('promedio');
    var arreglo = [];

    while(Listanumeros.options.length>0){
        Listanumeros.remove(0);
    }

    for(let con = 0; con < limite; con++) {
        let aleatorio = Math.floor(Math.random()*(50)+1); 
        Listanumeros.options[con] = new Option(aleatorio, 'valor:' + con);
        arreglo[con] = aleatorio;
    
    }

    menor.innerHTML = valorMenor(arreglo);
    menorPos.innerHTML = valorMenorPos(arreglo);
    mayor.innerHTML = valorMayor(arreglo);
    mayorPos.innerHTML = valorMayorPos(arreglo);

    promedio.innerHTML = valorPromedio(arreglo);

    par.innerHTML = porcentajePar(arreglo).toFixed(2) + "%"; 
    imp.innerHTML = porcentajeImpar(arreglo).toFixed(2) + "%";
    
    let pares = porcentajePar(arreglo);
    let impares = porcentajeImpar(arreglo);

    if(pares - impares > 25 || impares - pares > 25){
        simetria.innerHTML = "No";
    }else {
        simetria.innerHTML = "Sí";
    }

}

function saltarError(){
    valor = document.getElementById('limite').value;
    txtPares = document.getElementById('pares');
    txtImpares = document.getElementById('impares');
    txtSimetrico = document.getElementById('simetrico');
    txtMenor = document.getElementById('menor');
    txtMenorPos = document.getElementById('menorPos');
    txtMayor = document.getElementById('mayor');
    txtMayorPos = document.getElementById('mayorPos');
    txtPromedio = document.getElementById('promedio');

    if(valor==0){
        alert("Favor de capturar la cantidad de números");
        txtPares.innerHTML = "";
        txtImpares.innerHTML = "";
        txtSimetrico.innerHTML = "";
        txtMenor.innerHTML = "";
        txtMenorPos.innerHTML = "";
        txtMayor.innerHTML = "";
        txtMayorPos.innerHTML = "";
        txtPromedio.innerHTML = "";
    }
}

function valorMenor(numeros){

    let menorVal = numeros[0];

    for(let x = 0;x < numeros.length;++x){
        if(menorVal >= numeros[x]){
            menorVal = numeros[x];
        }
    }

    return menorVal;

}

function valorMenorPos(numeros){

    let menorVal = numeros[0];
    let menorValPos = 0;

    for(let x = 0;x < numeros.length;++x){
        if(menorVal >= numeros[x]){
            menorVal = numeros[x];
            menorValPos = x;
        }
    }

    return menorValPos;

}

function valorMayor(numeros){

    let mayorVal = 0;

    for(let x = 0;x < numeros.length;++x){
        if(mayorVal <= numeros[x]){
            mayorVal = numeros[x];
        }
    }

    return mayorVal;

}

function valorMayorPos(numeros){

    let mayorVal = 0;
    let mayorValPos = 0;

    for(let x = 0;x < numeros.length;++x){
        if(mayorVal <= numeros[x]){
            mayorVal = numeros[x];
            mayorValPos = x;
        }
    }

    return mayorValPos;

}

function valorPromedio(numeros) {
  
    let suma = 0;
  
    for (let x = 0; x < numeros.length; ++x) {
      suma += numeros[x];
    }
  
    const promedio = suma / numeros.length;
    return promedio;

  }

function porcentajePar(numeros){
    
    let x = 0; 
    let arregloRandom = numeros;

    for(let i=0; i<numeros.length; i++){
        if(arregloRandom[i]%2 == 0){ 

            x++;

        }
    }

    x = ((x * 100) / numeros.length);
    return x;

}

function porcentajeImpar(numeros){

    let x = 0; 
    let arregloRandom = numeros;

    for(let i=0; i < numeros.length; i++){
        if(arregloRandom[i]%2 != 0){ 

            x++;

        }
    }
    x = ((x * 100) / numeros.length);
    return x;

}